export const state = () => ({
  location: {},
  doctor: {},
  healthFacility: {}
})

export const getters = {
  location: (state) => {
    return state.location
  },
  doctor: (state) => {
    return state.doctor
  },
  healthFacility: (state) => {
    return state.healthFacility
  }
}

export const mutations = {
  SET_LOCATION: (state, data) => {
    state.location = data
  },
  SET_DOCTOR: (state, data) => {
    state.doctor = data
  },
  SET_HEALTH_FACILITY: (state, data) => {
    state.healthFacility = data
  }
}

export const actions = {

}
