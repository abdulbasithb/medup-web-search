export default {
  mode: 'universal',
  env: {
    baseUrl:
      process.env.NODE_ENV === 'dev'
        ? 'https://api-dev.medup.id/v2'
        : 'https://api.medup.id/v2'
  },
  /*
   ** Headers of the page
   */
  head: {
    title: 'MedUp',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    { src: '@/assets/scss/main.scss', lang: 'scss' }
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~plugins/leaflet.js', ssr: false },
    { src: '~plugins/ga.js', mode: 'client' }
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/eslint-module',
    'nuxt-purgecss',
    'nuxt-webfontloader',
    '@nuxtjs/dotenv'
  ],
  webfontloader: {
    google: {
      families: ['Lato&display=swap'] // Loads Lato font
    }
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    vendor: ['leaflet'],
    /*
     ** You can extend webpack config here
     */
    extend (config, ctx) { }
  },
  /*
  ** Router configuration
  */
  router: {

  },
  manifest: {
    short_name: 'MedUp',
    name: 'MedUp',
    background_color: '#FFF',
    display: 'standalone',
    theme_color: '#FFF'
  }
}
